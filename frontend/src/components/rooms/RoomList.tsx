import React, { useEffect, useState } from "react";
import { useAuth0 } from "@auth0/auth0-react";

import API from "../../utils/API"

const RoomList = () => {
  const { user, getAccessTokenSilently } = useAuth0();
  const [rooms, setRooms] = useState<any>(null);

  useEffect(() => {
    (async () => {
      try {
        const token = await getAccessTokenSilently({
          audience: "https://pubtrivia.mukherg.com/",
          scope: "read:posts",
        });
        let rooms = await API.get(
            `rooms/${user.name}`, {
                headers: {
                    "Authorization": `Bearer ${token}`
                }
            })
        setRooms(rooms);
      } catch (e) {
        console.error(e);
      }
    })();
  }, [getAccessTokenSilently, user.name]);

  return (
    <table className="table">
      <thead>
        <tr>
          <th scope="col">Creation Date</th>
          <th scope="col">Room Code</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>{rooms}</td>
          <td></td>
        </tr>
      </tbody>
    </table>
  );
};

export default RoomList;
