import React from "react";
import RoomList from "./RoomList";
import { useAuth0 } from "@auth0/auth0-react";

const Home = () => {
  const { isAuthenticated } = useAuth0();
    return (
        <div>
            {isAuthenticated && <RoomList />}
        </div>
    )
}

export default Home
