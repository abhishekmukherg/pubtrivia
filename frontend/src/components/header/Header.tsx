import React from "react";
import LoginButton from "./LoginButton";
import { useAuth0 } from "@auth0/auth0-react";
import LogoutButton from "./LogoutButton";


const Header = () => {
  const { isAuthenticated } = useAuth0();
  return (
    <div className="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
      <h5 className="my-0 mr-md-auto font-weight-normal">PubTrivia</h5>
      {!isAuthenticated ? <LoginButton /> : <LogoutButton />}
    </div>
  );
};

export default Header;
