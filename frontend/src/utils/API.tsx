import axios from "axios";


const API = axios.create({
  baseURL: "http://localhost:4503/api/",
  responseType: "json"
});

export default API
