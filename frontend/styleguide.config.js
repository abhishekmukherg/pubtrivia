module.exports = {
  components: "src/components/**/*.tsx",
  template: {
    head: {
      links: [
        {
          rel: 'stylesheet',
          href:
            'https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css'
        }
      ]
    }
  }
};
