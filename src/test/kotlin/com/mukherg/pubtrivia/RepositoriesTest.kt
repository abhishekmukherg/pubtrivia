package com.mukherg.pubtrivia

import com.natpryce.hamkrest.assertion.assertThat
import com.natpryce.hamkrest.greaterThanOrEqualTo
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.data.domain.PageRequest

@DataJpaTest
@AutoConfigureTestDatabase(replace = NONE)
class UserRepositoryTest {

    @Autowired
    lateinit var repository: UserRepository

    @Test
    fun `can create user`() {
        val u = repository.save(
            User("test-username")
        )
        assertThat(u.id, greaterThanOrEqualTo(1L))
        assertEquals("test-username", u.username)
        assertNotNull(u.createdDate)
        assertNotNull(u.lastModifiedDate)
    }

    @Test
    fun `cannot find by username`() {
        assertNull(repository.findByUsername("invalid"))
    }
}
@DataJpaTest
@AutoConfigureTestDatabase(replace = NONE)
class RoomRepositoryTest {

    @Autowired
    lateinit var users: UserRepository

    @Autowired
    lateinit var rooms: RoomRepository

    @Test
    fun `find by non-existent user`() {
        val page = PageRequest.of(0, 10)
        val p = rooms.findByCreatedBy(0L, page)
        assertEquals(p.totalElements, 0L)
    }

    @Test
    fun `create room for user`() {
        val u = users.save(User("room-repository-create-room-for-user"))
        rooms.save(
            Room("abcd", u.id)
        )
    }
}
