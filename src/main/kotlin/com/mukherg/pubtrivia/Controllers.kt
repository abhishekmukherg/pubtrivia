package com.mukherg.pubtrivia

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.server.ResponseStatusException
import java.lang.Integer.min
import java.security.Principal

@RestController("/users")
class QuestionControllers {
    @GetMapping("/displayed")
    fun displayed(): List<Question> {
        return listOf()
    }
}

@RestController
class RoomsController(
    @Autowired val roomsRepository: RoomRepository,
    @Autowired val userRepository: UserRepository,
    @Value("\${pubtrivia.maxPageSize}") val maxPageSize: Int
) {
    @GetMapping("/rooms/{user}")
    fun list(
        @PathVariable("user") username: String,
        @RequestParam(required = false, defaultValue = "0", name="page") pageNum: Int,
        @RequestParam(required = false, defaultValue = "10") pageSize: Int,
        principal: Principal
    ): Page<Room> {
        if (principal.name != username) throw ResponseStatusException(HttpStatus.FORBIDDEN)
        val user = userRepository.findByUsername(username) ?: User(username = username).let { userRepository.save(it) }

        val page = PageRequest.of(pageNum, min(pageSize, maxPageSize))
        return roomsRepository.findByCreatedBy(user.id, page)
    }
}
