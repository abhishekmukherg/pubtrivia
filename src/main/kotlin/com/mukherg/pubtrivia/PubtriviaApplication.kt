package com.mukherg.pubtrivia

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class PubtriviaApplication

fun main(args: Array<String>) {
	runApplication<PubtriviaApplication>(*args)
}

