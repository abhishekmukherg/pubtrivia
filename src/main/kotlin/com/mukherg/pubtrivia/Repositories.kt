package com.mukherg.pubtrivia

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.PagingAndSortingRepository

interface UserRepository : CrudRepository<User, Long> {
    fun findByUsername(username: String): User?
}

interface RoomRepository: PagingAndSortingRepository<Room, Long> {
    fun findByCreatedBy(createdBy: Long, pageable: Pageable): Page<Room>
}
