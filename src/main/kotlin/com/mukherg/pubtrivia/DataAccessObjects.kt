package com.mukherg.pubtrivia

import org.springframework.data.annotation.CreatedBy
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import java.time.OffsetDateTime
import java.time.ZoneOffset
import javax.persistence.*

@Entity
@Table(name = "questions")
data class Question(
    @Id
    @GeneratedValue
    val id: Long,

    @Column(nullable = false)
    val text: String,

    @Column(nullable = false)
    val displayed: Boolean
)

@Entity
@Table(name = "users")
data class User(
    @Column(nullable = false)
    val username: String,

    @CreatedDate
    @Column(columnDefinition = "TIMESTAMP WITH TIME ZONE")
    val createdDate: OffsetDateTime = OffsetDateTime.now(ZoneOffset.UTC),

    @LastModifiedDate
    @Column(columnDefinition = "TIMESTAMP WITH TIME ZONE")
    val lastModifiedDate: OffsetDateTime = OffsetDateTime.now(ZoneOffset.UTC)
) {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "users_id_seq")
    @SequenceGenerator(name = "users_id_seq", sequenceName = "users_id_seq")
    val id: Long = 0
}

@Entity
@Table(name = "rooms")
data class Room(
    @Column(nullable = false)
    val code: String,

    @CreatedBy
    @Column(nullable = false)
    val createdBy: Long,

    @CreatedDate
    @Column(columnDefinition = "TIMESTAMP WITH TIME ZONE")
    val createdDate: OffsetDateTime = OffsetDateTime.now(ZoneOffset.UTC)
) {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "rooms_id_seq")
    @SequenceGenerator(name = "rooms_id_seq", sequenceName = "rooms_id_seq")
    val id: Long = 0
}
