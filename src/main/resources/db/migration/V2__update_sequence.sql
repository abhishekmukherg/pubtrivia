ALTER SEQUENCE correctanswers_id_seq INCREMENT BY 50;
ALTER SEQUENCE questions_id_seq      INCREMENT BY 50;
ALTER SEQUENCE roomadmins_id_seq     INCREMENT BY 50;
ALTER SEQUENCE rooms_id_seq          INCREMENT BY 50;
ALTER SEQUENCE teams_id_seq          INCREMENT BY 50;
ALTER SEQUENCE users_id_seq          INCREMENT BY 50;
