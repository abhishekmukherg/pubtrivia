CREATE TABLE users(
    id BIGSERIAL PRIMARY KEY,
    username VARCHAR(32) UNIQUE NOT NULL,
    created_date TIMESTAMP WITH TIME ZONE NOT NULL,
    last_modified_date TIMESTAMP WITH TIME ZONE NOT NULL
);

CREATE TABLE rooms(
    id BIGSERIAL PRIMARY KEY,
    code VARCHAR(4) NOT NULL,
    created_date TIMESTAMP WITH TIME ZONE NOT NULL,
    created_by BIGINT NOT NULL REFERENCES users(id)
);

CREATE TABLE roomAdmins(
    id BIGSERIAL PRIMARY KEY,
    room_id BIGINT REFERENCES rooms(id) NOT NULL,
    users BIGINT REFERENCES users(id) NOT NULL,
    UNIQUE (room_id, users)
);

CREATE TABLE questions(
    id BIGSERIAL PRIMARY KEY,
    question_text TEXT
);

CREATE TABLE rooms_questions(
    room_id BIGINT REFERENCES rooms(id) NOT NULL,
    question_id BIGINT REFERENCES questions(id) NOT NULL,
    place INT NOT NULL,
    PRIMARY KEY (room_id, question_id, place)
);

CREATE TABLE correctAnswers(
    id BIGSERIAL PRIMARY KEY,
    question_id BIGINT REFERENCES questions(id) NOT NULL,
    answer TEXT NOT NULL,
    UNIQUE (question_id, answer)
);

CREATE TABLE teams(
    id BIGSERIAL PRIMARY KEY,
    room_id BIGINT REFERENCES rooms(id) NOT NULL,
    team_name VARCHAR(32) NOT NULL,
    UNIQUE (room_id, team_name)
);
